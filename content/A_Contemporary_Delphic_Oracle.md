Title: A Contemporary Delphic Oracle
Date: 2016-10-04
Tags: big data, climate change, singularity, technology, magic

Today [Furtherfield](http://furtherfield.org) published my essay on waiting for the technological rapture in the church of big data. The article is about the paralysing effect of hiding the human hand in software through anthropomorphising computers and dehumanising ourselves.

You can read the article [here](http://www.furtherfield.org/features/contemporary-delphic-oracle-church-big-data). 

![](images/contemporary-delphic-oracle.png)
