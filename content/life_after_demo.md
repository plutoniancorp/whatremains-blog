Title: Life After Demo!
Date: 2017-05-18
Tags: news, dev

![](images/controller.png)

Good news! There is life after demo! Thanks to the wonderful support from [Creative Industries Fund NL](http://stimuleringsfonds.nl/), [Democracy &#38; Media Foundation](https://www.stdem.org/), [WORM](https://worm.org/), [V2&#95; Lab for the Unstable Media](http://v2.nl/) and [Servus](https://core.servus.at/), we can start working on the development of the game! We will start with a sprint to work on the main story, development environment and infrastructure. After that we will focus on implementing each stage, testing with an audience and using the feedback to improve completed stages. If all goes according to plan, you can play the complete game at the end of December 2017. 


