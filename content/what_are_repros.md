Title: What are NES Game Reproductions?
Date: 2016-09-23
Tags: hardware

In our project,
we are investigating the possibility of publishing the game not just as a single ROM file to load on emulators and flash cartridges,
but also in the form of a limited series of physical cartridges.
So even if emulators and flash carts are useful tools to both develop,
test,
and support the digital distribution of the game,
we also need to start thinking about the hardware aspect.
What we need to figure out in particular,
is whether or not we can produce this limited series using solely recycled material,
so as to fit with [the scope of the project]({filename}pages/about.md).
We will soon explain and document our first attempt at making such a simple recycle cartridge.
However,
before that we need to talk a bit about cartridge reproductions...


![Everdrive flashcart menu showing some test ROMs]({filename}images/everdrive_menu.jpg)


To develop the software,
we are essentially using NES/FC emulators.
However it is quite important to also test on the real hardware.
For that,
so-called flash cartridges can be used,
and every now and then we test the game engine on such special cartridges because emulators might not work the same way as the real hardware in some specific situations,
or instead,
may be more forgiving in others.
Flash cartridges are really interesting pieces of hardware (really).
They are basically cartridges with either onboard flash memory or a slot for memory cards (like Compact Flash, SD Cards, etc),
on which Mega- and Gigabytes of games and homebrew software can be stored,
organised,
selected,
and accessed by the hardware console.
The design of such carts is not trivial as it does not simply need to pretend it is a regular cart,
it also needs to emulate the hardware of the cart.
That's an important aspect,
because if you remember the discussion about mappers,
[in an earlier post]({filename}the_thing_about_making_a_nes_cartridge.md),
there are a lot of different hardware designs for game cartridges.
So you can easily deduct that in order to have a universal flash cart,
such cart would have to be able to emulate a rather broad range of all sort of different pieces of hardware.


![Everdrive mappers]({filename}images/mappers.png)


In practice though,
flash cartridges are not universal.
Once a ROM file is loaded in the flash cart,
it is analysed,
for instance by reading the headers of the ROM image file---more on that part later,
and then the flash cart chooses the correct mapper it needs to emulate for the software to work properly.
Once again,
there are lots of mappers,
so this is the reason why it is common for flashcarts manufacturers to list the mappers that are compatible,
and,
understandably,
these manufacturers are more likely to support the most common commercial mappers instead of obscure bootleg,
unlicensed,
or homebrew ones.
So yes,
*Super Mario Bros.* will work,
but not something like *RacerMate Challenge II*,
an obscure [bike trainer game and set of equipment](http://www.nesmuseum.com/racermate.html).
But this can be also a problem for cartridges that seem to just hold regular games.


![Gimmick! Famicom box]({filename}images/gimmick_famicom_box.jpg)


To give an example,
the platform game *Gimmick!* contains the Sunsoft 5B mapper chip,
that next to PRG/CHR extended addressing capabilities,
also provides extra audio features similar to the Yamaha YM2149 chip found notably in the Atari ST home computer--and used for [plethora of great chiptunes](http://sndh.atari.org/sndh/browser/index.php?dir=sndh_lf/).
In *Gimmick!*,
it is used to add a few more tracks to the game background music as well as to generate some extra sound effects[^1].
If the flashcart does not emulate the soundchip part of the Sunsoft 5B mapper chip,
the game will work,
albeit with incomplete audio.
This was the case for the Everdrive N8 flash cart.
However,
thanks to the availability of tech documentation for this cartridge and a design that allowed the loading of user made mappers[^2],
a member of the Everdrive user community eventually contributed a complete Sunsoft 5A/5B/FME-7 Mapper implementation that permitted to experience *Gimmick!* fully even when loaded from the flash cartridge[^3].


<iframe width="100%" src="https://www.youtube.com/embed/NhR3oukQaI4" frameborder="0" allowfullscreen></iframe>


As a result something interesting happens.
The point of a flashcart is to play copied games and homebrew software on the real hardware.
That's the case when the mapper emulated only concerns allocation and addressing of memory,
but if some other parts of the cart becomes emulated,
like the soundchip,
you end up in practice with some sort of hybrid system of partly emulated,
partly real,
computation.
Because of this some people prefer to play the *real* cartridge[^4].
That's cool,
NES/FC games are cheap,
no big deal.
Well,
actually,
no,
not cool.
Some NES/FC can be outrageously expensive for all sorts of reasons,
and even though *Gimmick!* is not a rare piece of dubious apartment TV bike training system that collectors might feel they need to own immediately,
it is a rare game nonetheless,
and therefore quite expensive to acquire.
So,
depending on the state of the cart,
and whether you'd like it with box and manual,
the PAL NES version or the NTSC Famicom version,
if you purchase in specialised communities or on mainstream auction sites,
etc,
you can count spending between 250€ and 1000€ to play the real cartridge.
Wat do?


![Gimmick vs Batman]({filename}images/gimmick_batman.jpg)


Enter the wonderful world of *reproductions*,
also known as *repros*.
The nice thing about having games sharing the same mappers and cart designs,
is that the data they carry can be simply swapped.
So you can open up a cart and change the Mask read-only memory (ROM) chips with the one from another game,
as long as their carts are compatible.
But wait,
there's more!
Instead of replacing Mask ROMs with other Mask ROMs you can replace them with [EPROM](https://en.wikipedia.org/wiki/EPROM) or [EEPROM](https://en.wikipedia.org/wiki/EEPROM) chips,
which content can be rewritten.
You should start seeing where this is going.
See,
still taking *Gimmick!* as an example,
there is another game that uses the same mapper and cart design:
*Batman: Return of the Joker*,
which,
at 20€ loose,
is way more affordable than the rare platformer[^5].
In repro terms,
this game is called a *donor cart*,
as in ... organ donor...
So if someone really wants to play *Gimmick!* on the real hardware,
with the real cart,
and its real soundchip,
then it is as easy as opening the *Batman* cartridge,
desolder the Mask ROM chips that contain the program and character graphics---more on that in another post---and replace them with compatible EPROM or EEPROM chips that have been flashed with the ROM images of *Gimmick!*.
That's it enjoy your real *Gimmick!*[^6].


![Mr. Gimmick repro]({filename}images/gimmick_repro.jpg)


Why telling all that?
For two reasons.
First,
our game cartridge prototype,
and by extension the idea to produce a limited series of recycled game carts for the project,
boils down in fact to the making of something similar to a repro.
The difference being that we will not flash a rare game,
but our own.
Thus we need to adopt a mapper and cart design as widespread as possible,
to only use cheap and easy to find donor carts with not so interesting games on them (YMMV).
So it's a bit the reverse order of things,
as instead of developing the game however we like and make the hardware to match,
we need to figure out which cart type is the most strategic to use,
and then develop the game within the constraint of its mapper and design.
Second,
and most importantly,
this whole undertaking would not be possible without the work of all those who have,
for many many years now,
carefully documented and share their findings while hacking and reverse engineering the NES and Famicom consoles.
So if this post made you interested in NES development,
we really recommend you to pay a visit to the great [NesDev community](http://nesdev.com) that maintains,
shares,
and keeps expanding much of the technical info surrounding the NES/FC hardware and software.


In a next post,
we'll cover the making of a simple game dev cart/proto derived from repro making techniques.

(PS: with repro you can also flash altered versions of existing games, translations and other ROM hacks. Check the [ROMhacking.net](http://www.romhacking.net) community!)


Image/Audio Credits
-------------------

* Everdrive N8 mapper table: [Krikzz](http://krikzz.com/pub/support/everdrive-n8/)
* Japanese Gimmick! box photo: some anon on /vr/
* Gimmick! OST capture: [explod2A03](https://www.youtube.com/watch?v=NhR3oukQaI4) (composed by Masashi Kageyama)
* Carts + PCB Mr. Gimmick and Batman: Return of the Joker: [NES Cart Database](http://bootgod.dyndns.org:7777/)
* Mr. Gimmick reproduction process: extracts from [Creating a Mr Gimmick NES Reproduction Cartridge - start to finish](https://www.youtube.com/watch?v=xRGJdU5u3cU) by RetroGameGirl/ThunderSqueak




[^1]: This is possible because of something called expansion audio,
a feature of the Famicom,
that permits the mixing and processing of the console's audio signal by the cartridge extra chip(s).
This feature was not available on the NES but it is possible to mod the latter to provide such feature.
See [Open Hidden Sound Channels in the NES](http://www.retrofixes.com/2014/05/open-hidden-sound-channels-in-nes.html).

[^2]: [http://krikzz.com/pub/support/everdrive-n8/development/](http://krikzz.com/pub/support/everdrive-n8/development/)

[^3]: [http://krikzz.com/forum/index.php?topic=3245.msg32176#msg32176](http://krikzz.com/forum/index.php?topic=3245.msg32176#msg32176)

[^4]: Game console hardware versus ROM emulation,
and similar debate like CRT screens versus LCD screens,
is a never ending source of heated discussions that I wish not to trigger here.
If you thought the free software versus open source software division was intense,
you have not seen anything yet.

[^5]: Actually,
this games used to be even cheaper,
but with everyone wanting to make *Gimmick!* repros,
it seems its value increased a lot in the past years.

[^6]: Yes,
collectors are screaming now,
it's not the real *Gimmick!*,
etc.
It's OK,
we're all going to die one day.
