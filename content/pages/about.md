Title: About the project...
Date: 2017-05-18

What Remains is a darkly humorous, authentic Nintendo Entertainment System (NES) 8-bit game based on how public opinion was, and still is, shaped to prevent the creation of government regulations needed to protect us from man-made environmental disasters. 

It is 1986. You just came home from a skate session when a Nintendo game cartridge falls into your hands. Excited to find out what kind of game is on the cartridge, you visit your friend who just bought a brand new Nintendo console two weeks ago. You plug in the cartridge and a strange list of encrypted files appears… 

What Remains is an art game for the emblematic 1985 NES video game console, immersing the player in an eighties adventure to save the planet. As the story unravels, you are confronted with the different methods used to spread false information and doubt, drawing parallels to today’s technologically enhanced media landscape that does not cause, but greatly facilitates the spread of false information. In line with the project’s concept, the game is developed on reused and repurposed cartridges.

In this context, the NES stands as a metaphor – a device from that same period, originally designed only for entertainment and consumption, now being repurposed as a tool for creative expression. Being nostalgically couched in the past, it acts as a symbol to that time’s influence on the present.

