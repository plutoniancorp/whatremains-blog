Title: Playtests @ V2_ 
Date: 2017-10-05
Tags: news, dev, playtest

![](images/boot_sequence_2.png)

Iodyne Dynamics, in collaboration with V2_ in Rotterdam, will host three playtests, and we are looking for players to come and give us feedback on the current prototype!

Come save the planet and join us the evening of the 6th of October, the 3rd of November and the 1st of December!

Part I: October 6, 2017 from 18.00 - 21.00h
Part II: November 3, 2017 from 18.00h - 21.00h
Part III: December 1, 2017 from 18.00h - 21.00h

Free entrance! This event is part of the Kunstavond.

\#3X3: THE THREE OF ...
---------------------------------------
Experiments are an essential part of every art practice. Even more than the end results, experiments, bèta versions and try-outs offer an inside look on how an artist thinks and works. Therefore, the experiment is made central in 3x3. V2_ offers three artists the space, time and resources to develop three experiments. Every first Friday evening of the month the public can witness one of these experiments live. A unique experience, since no one knows what experiments will lead to…

\#KUNSTAVOND
--------------
Every Friday the Kunstblock art venues in the Witte de Withkwartier are open in the evening. Visit MAMA, TENT, UBIK, V2_Lab for the Unstable Media, Witte de With Center for Contemporary Art and CBK Rotterdam’s collection of art in public space. Click here for the programme. 
