Title: Setting the Context – Climate Change Before the 1980s
Date: 2016-09-02
Tags: context, climate change


What Remains will be made for the Nintendo Entertainment System,
a robust piece of technology,
dating from 1983,
and seeing a resurgence in popularity.
In keeping with the climate change theme, 
and [as discussed in a previous post]({filename}the_thing_about_making_a_nes_cartridge.md),
the game cartridge will likely be scavenged and re-purposed using as much recycled materials as possible. 
The game will begin in the early 1980s,
and in order to help guide the decision about the storyline,
[Amber Griffiths](http://www.ambergriffiths.info/) has been looking into the scientific discoveries,
and the political,
cultural and environmental events that have shaped our collective understanding of,
and attitudes towards climate change.
To begin with this will be organised in two posts as a strict chronology – items on the list can probably be linked,
but Amber wanted to be careful about this as it is never straightforward.
The game will need to be designed very thoughtfully,
to avoid any hint of conspiracy theorising in either direction – the most appropriate way to do this is by simply presenting true events that have already happened.


Creating the chronology has made Amber fairly confident that the situation we are in has been caused by wholly unexceptional,
everyday daft decision-making,
and generally taking the easy/profitable options.
The challenge now is to curate the numerous and often rather dry events,
and create a game that remains interesting and ideally eye-opening for this particular aspect.
The game period for What Remains will run from 1983-2016,
so we need to look at the events leading up to this to properly understand the context that it sits within.
The following covers some of what Amber thought were important events in the run up to the 1980s.


In the 1800s,
scientists were just beginning to understand the effects of Greenhouse gases – John Tyndall determined that coal gas strongly absorbed infra-red radiation as early as 1859.
The industrial revolution was playing out,
leaving atmospheric CO<sub>2</sub> at ~280ppm and having kick started global warming.

![](images/NASA-Apollo8-Dec24-Earthrise.jpg)

It took until 1965 for global warming to be talked about by a major political figure – Lyndon Johnson (US president) was the earliest Amber could find.
In 1968 the Earthrise photograph was taken during the Apollo 8 mission,
and seeing our planet as a whole for the first time triggered a huge rise in new environmental movements and charities,
including Greenpeace in 1971.
Environmental reporting started to ramp up.
Around the same time (1969) the Murdoch media empire began to [expand globally](https://en.wikipedia.org/wiki/Rupert_Murdoch) - something that would later become important.


In 1972 John Sawyer summarised our scientific knowledge to date, predicting something we now know to be a vast underestimation:


> The increase of 25% CO<sub>2</sub> expected by the end of the century therefore corresponds to an increase of 0.6°C in the world temperature – an amount somewhat greater than the climatic variation of recent centuries


A major global event then began in 1973 as James Lovelock speculated that chlorofluorocarbons (CFCs) could have a global warming effect,
and in the early 1970s CFCs were also shown to deplete the ozone layer.
At that time,
CFCs were widely used as refrigerants,
propellants in aerosols, and solvents [Note from Amber: I'll cheat a bit here and skip to the 1980s for the sake of a more complete story].
A global solution was sought,
and an international treaty (the [Montreal Protocol](http://www.ozone.unep.org/en/treaties-and-decisions/montreal-protocol-substances-deplete-ozone-layer)) was agreed in 1987 and enforced from 1989 – to go from the scientific evidence to global policy change in <15 years was remarkable,
and a first.


Meanwhile,
in the midst of this global policy success,
the first models of global climate change were being developed.
In 1975 [Manabe and Wetherald](http://journals.ametsoc.org/doi/abs/10.1175/1520-0469(1975)032%3C0003:TEODTC%3E2.0.CO;2) showed that doubling CO<sub>2</sub> in the atmosphere was expected to lead to ~2°C global temperature rise (more serious than earlier predictions,
but still a dramatic underestimation).
At this point, global CO<sub>2</sub> concentrations were quickly increasing and had reached ~330ppm,
while the maximum recorded level over the previous 800,000 years was ~300ppm.
In 1979,
the [World Climate Conference of the World Meteorological Organization](http://www.dgvn.de/fileadmin/user_upload/DOKUMENTE/WCC-3/Declaration_WCC1.pdf) concluded that change was closer than we thought:


> some effects on a regional and global scale may be detectable before the end of this century and become significant before the middle of the next century


A later [survey](http://journals.ametsoc.org/doi/pdf/10.1175/2008BAMS2370.1) of the scientific literature from 1965-1979 found 7 articles predicting global cooling,
and 44 predicting global warming – indicating that at this time,
the trend was strong but not yet a consensus.
Somewhat unrelated,
in response to oil price increases,
the technology that could later help address the problem was already being developed - NASA,
together with the US Department of Energy and the Department of Interior began a program in 1975 to start building and testing turbines for utility scale electricity generation.


![](images/2000px-Wind_generator_comparison.svg.png)


In 1982,
just before the Nintendo Entertainment System was launched and our game begins,
[Greenland ice cores were drilled](http://science.sciencemag.org/content/218/4579/1273).
The cores showed that dramatic temperature changes (like the onset of ice ages) had happened before in the space of a only century – we then knew that very severe climate change was possible within a human lifetime.


Despite the success story of the Montreal Protocol for CFCs showing that global action in response to scientific evidence is possible in a short time scale,
we have not been so effective in our response to climate change.
It has been over 40 years since we had strong scientific evidence of the problems caused by CO<sub>2</sub> emissions.
Climate change is,
clearly,
a far harder problem to address than that of CFCs - requiring substantial changes in the way of life of those who hold global power.
Over the time that the game takes place (1980s-2016),
we see some embryonic power shifts begin.


To be continued.


Image Credits
-------------

* Earthrise: NASA / Bill Anders (public domain)
* Wind generator comparison: TastyCakes on English Wikipedia (CC-BY 3.0 Unported)

