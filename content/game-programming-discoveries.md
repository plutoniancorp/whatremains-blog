Title: NES/Famicom game programming discoveries
Date: 2016-09-30
Tags: compilers, Famicom, lisp, NES, sprites, PPU, tiles

Working on a NES game you are treading in the footsteps of programmers
from the 80's, and going back to modern development feels strangely bloated and
inefficient in comparison. This is a log of some of the things I've encountered
writing game code for the What Remains project.

Firstly, although it seemed like a lunatic idea at the start, I'm very
happy we decided to build a compiler first so I could use a high level
language (Lisp) and compile it to 6502 assembler that the NES/Famicom
needs to run. This has made it so much faster to get stuff done, and
for example to be able to optimise the compiler for what is needed as
we go along, without having to change any game code. I'm thinking
about how to bring this idea to less projects on less esoteric
hardware.

These are the odds and ends that I've built into the game, and some of
the reasons for decisions we've made.

![](images/dis-1.png)

##Collisions

As well as sprite drawing hardware, later games machines (such as the
Amiga) had circuitry to automatically calculate collisions between
sprites and also background tiles. There is a feature on the NES that
does this for only the first sprite - but it turns out this is more
for other esoteric needs, for normal collisions you need to do your
own checks. For background collisions to prevent walking through
walls, we have a list of bounding boxes (originally obtained by
drawing regions over screenshots in gimp) for each of the two map
screens we're building for the demo. We're checking a series of
'hotspots' on the character against these bounding boxes depending on
which way you are moving in, shown in yellow below.

![](images/hotspots.png)

You can also check for collisions between two sprites - all the
sprites we're using are 2x2 'metasprites' as shown above as these are
really the right size for players and characters, as used in most
games. These collisions at the moment just trigger animations or a
'talk mode' with one of the characters.

Entity system

With the addition of scrolling and also thinking about how to do game
mechanics, it became apparent that we needed a system for dealing with
characters, including the player - normally this is called an entity
system. The first problem is that with multiple screens and scrolling,
the character position in the world is different to that of the screen
position which you need to give to the sprites. The NES screens are
256 pixels wide, and we are using two screens side by side, so the
'world position' of a character in the x axis can be from 0 to
512. The NES is an 8 bit console, and this range requires a 16 bit
value to store. We also need to be able to turn off sprites when they
are not visible due to scrolling, otherwise they pop back on the other
side of the screen. The way we do this is to store a list of
characters, or entities, each of which contain five bytes:

   - Sprite ID (the first of the 4 sprites representing this entity)
   - X world position (low byte)
   - X world position (high byte) - ends up 0 or 1, equivalent to which screen the entity is on
   - Y world position (only one byte needed as we are not scrolling vertically)
   - Entity state (this is game dependant and can be used for 8 values such as "have I spoken to the player yet" or "am I holding a key")

We are already buffering the sprite data in "shadow memory" which we
upload to the PPU at the start of every frame, the entity list
provides a second level of indirection. Each time the game updates, it
loops through every entity converting the world position to screen
position by checking the current scroll value. It can also see if the
sprite is too far away and needs clipping - the simplest way to do
that seems to be simply setting the Y position off the bottom of the
screen. In future we can use the state byte to store animation frames
or game data - or quite easily add more of these as needed.

![](images/dis-2.png)

##Sound

Obviously I was itching to write a custom sound driver to do all sorts
of crazy stuff, but time is very limited and it's better to have
something which is compatible with existing tracker software. So we
gave in and used an 'off the shelf' open source sound driver. First I
tried ggsound but this was a bit too demanding on memory and I
couldn't get it playing back music without glitching. The second one I
tried was famitone which worked quite quickly, the only problem I had
was with the python export program! It only requires 3 bytes of zero
page memory, which is quite simple to get the Lisp compiler to reserve
- and a bit of RAM which you can easily configure.

##New display primitives

I wrote before about how you need to use a display list to jam all the
graphics data transfer into the first part of each frame, the precious
time while the PPU is inactive and can be updated. As the project went
on there were a couple more primitive drawing calls I needed to add to
our graphics driver.

To display our RPG game world tiles, the easiest way is putting big
lists describing an entire screen worth of tiles into PRG-ROM. These
take up quite a bit of space, but we can use the display list to
transfer them chunk by chunk to the PPU without needing any RAM, just
the ROM address in the display list. I'm expecting this will be useful
for the PETSCII section too.

I also realised that any PPU transfer ideally needs to be scheduled by
the display list to avoid conflicts, so I also added a palette switch
primitive too. We are switching palettes quite a lot, between each
game mode (we currently have 3, intro, RPG and the PETSCII demo) but
we're also using an entirely black palette to hide the screen refresh
between these modes. So the last thing we do in the game mode switch
process (which takes several frames) is set the correct palette to
make everything appear at once.

##Memory mapping

By default you get 8K of data to store your graphics - this means 256
sprites and 256 tiles only. In practice this is not enough to do much
more than very simple games, and we needed more than this even for our
demo. The fact that games are shipped as hardware cartridges meant
that this could be expanded fairly simply - so most games use a memory
mapper to switch between banks of graphics data - and also code can be
switched too.

There are hundreds of variants of this technique employing different
hardware, but based on Aymeric's cartridge reverse engineering we
settled on MMC1 - one of the most common mappers.

In use this turns out to be quite simple - you can still only use 256
tiles/sprites at a time, but you can switch between lots of different
sets, something else that happens between game modes. With MMC1 you
just write repeatedly to an address outside of the normal range to
talk to the mapper via serial communication - different address ranges
control different things.

![](images/dis-3.png)
